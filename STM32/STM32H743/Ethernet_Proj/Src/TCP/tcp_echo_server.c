/**
* This source file is made for testing tcp/ip communication
* 
* 
* @author Hyun-Ho Cha
* @version 1.0 initial add tcp application
* @see None
*/

static void tcpecho_thread(void *arg)
{
  /* Create a new connection identifier. */
  conn = netconn_new(NETCONN_TCP);
  if (conn!=NULL)
  {
   /* Bind connection to well known port number 7. */
   err = netconn_bind(conn, NULL, 7);
     if (err == ERR_OK)
     {
       /* Tell connection to go into listening mode. */
       netconn_listen(conn);
       while (1)
       {
         /* Grab new connection. */
         accept_err = netconn_accept(conn, &newconn);

         /* Process the new connection. */
         if (accept_err == ERR_OK)
        {
           while (( recv_err = netconn_recv(newconn, &buf)) == ERR_OK)
           {
              do
             {
               netbuf_data(buf, &data, &len);
               netconn_write(newconn, data, len, NETCONN_COPY);
             }
             while (netbuf_next(buf) >= 0);

            netbuf_delete(buf);
          }
             /* Close connection and discard connection identifier. */
             netconn_close(newconn);
             netconn_delete(newconn);
        }
      }
    }
    else
    {
      netconn_delete(newconn);
    }
  }
}